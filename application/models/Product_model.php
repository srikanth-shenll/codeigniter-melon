<?php
class Product_model extends  CI_Model {
    function __construct()  {
        parent::__construct();
         $this->load->helper('url');
          $this->load->library(array('ion_auth', 'session'));
          $this->lang->load('auth');
    }
	public function getProducts() {       // Getting all Products from the data base
		$this->db->select("*");
		$this->db->from('tbl_products');
		$this->db->order_by('product_id', 'DESC');
		$query = $this->db->get();
		return $query->result();		 
	} 
	
	public function deleteProduct($id) {  //  Delete  particular id from the data base
	    $this->db->where('product_id', $id);
	     $this->db->delete('tbl_products');
	    if ($this->db->affected_rows() == 1)  return TRUE;
	    else  return FALSE;
	}

	public function selectProductById($id) {
			$id = intval($id);
			$select_sql="select * from tbl_products where product_id=".$id;
			$repeat_product = $this->db->query($select_sql);
			if($repeat_product->num_rows() > 0){
				$products = $repeat_product->result();
				return $products[0];	
			}else
				return false;
	}
	public function getProductBySku($Sku_products) {
			$select_sql="select sku from tbl_products where sku ='$Sku_products'";
			$repeat_product=$this->db->query($select_sql);
			$products=$repeat_product->num_rows();
			return $products;	
	}
	public function getProductBySkuId($Sku_products,$id) {
			$select_sql1="select sku from tbl_products where sku ='$Sku_products' and product_id='$id'";
			$repeat_product1=$this->db->query($select_sql1);
			$products1=$repeat_product1->num_rows();
			if ($repeat_product1->num_rows() > 0) {
			   foreach ($repeat_product1->result() as $row) {
			      $updated_sku= $row->sku; 
			   }
			}
			return $updated_sku;	
	}
	public function updateProduct($Sku_products,$description_products,$datetime,$id) {
		$sql ="UPDATE tbl_products SET sku='$Sku_products', description='$description_products',modified_date='$datetime' WHERE product_id='$id'";
		$this->db->query($sql);
	}

	public function addProduct($Sku_products,$description_products,$datetime) {
		$sql = "INSERT INTO tbl_products ".
       "(sku,description,product_type,created_date,modified_date) ".
       "VALUES ".
       "('$Sku_products','$description_products','product','$datetime','$datetime')";
		 $this->db->query($sql);
	}
}
