<?php
/*
 +-++-++-++-++-++-+ +-++-++-++-++-++-++-++-+ +-++-++-++-++-++-++-++-++-+
 |S||H||E||N||L||L| |S||O||F||T||W||A||R||E| |S||O||L||U||T||I||O||N||S|
 +-++-++-++-++-++-+ +-++-++-++-++-++-++-++-+ +-++-++-++-++-++-++-++-++-+
*/
function showAlertMessage($type, $message, $html = false){

	if(empty($message) || is_array($message)){
		return false;
	}

	switch($type){
		case 'success':
			$class = 'alert-success';
	 	break;
	 	case 'info':
	  		$class = 'alert-info';
	 	break;
	 	case 'warning':
	  		$class = 'alert-warning';
	 	break;
	 	case 'error':
	 	case 'danger':
	  		$class = 'alert-danger';
	 	break;
		default:
	  		$class = 'alert-info';
	}

	$message = ($html === true)?$message:htmlentities($message);
	$rtnMessage = '<div class="alert '.$class.' alert-dismissable">
    <i class="icon-remove close" data-dismiss="alert"></i>
     '.$message.'
 	</div>';

	return $rtnMessage;
}