<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sample extends CI_Controller {
  public function __construct()
  {
          parent::__construct();
          $this->load->helper('url');
          $this->load->library(array('ion_auth', 'session'));
          $this->lang->load('auth');
  }

  public function index() {
    $this->securityCheck();
  	$this->_render_page('sample/form');
  }

  public function form() {
    $this->securityCheck();
    $this->_render_page('sample/form');
  }

  public function listing() {
    $this->securityCheck();
    $this->_render_page('sample/listing');
  }

  public function blank()  {
    $this->securityCheck();
    $this->_render_page('sample/blank-page');
  }

  public function dashboard()  {
    $this->securityCheck();
    $this->_render_page('sample/blank-page');
  }

  public function generateExcel() {
    $this->securityCheck();
    $objPHPExcel = new PHPExcel();
    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
                   ->setLastModifiedBy("Maarten Balliauw")
                   ->setTitle("PHPExcel Test Document")
                   ->setSubject("PHPExcel Test Document")
                   ->setDescription("Test document for PHPExcel, generated using PHP classes.")
                   ->setKeywords("office PHPExcel php")
                   ->setCategory("Test result file"); 


    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Hello')
                ->setCellValue('B2', 'world!')
                ->setCellValue('C1', 'Hello')
                ->setCellValue('D2', 'world!');

    // Miscellaneous glyphs, UTF-8
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A4', 'Miscellaneous glyphs');


    $objPHPExcel->getActiveSheet()->setCellValue('A8',"Hello\nWorld");
    $objPHPExcel->getActiveSheet()->getRowDimension(8)->setRowHeight(-1);
    $objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setWrapText(true);
    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Simple');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    // Save Excel 2007 file
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $filename = BASEPATH.'/../uploads/sample.xlsx';
    $objWriter->save($filename);
    die('File generated in uploads');
  }
  private function securityCheck() {
    if(!$this->ion_auth->logged_in()){
      redirect('auth/login', 'location');
    }else{
      return true;
    }
  }

  public function _render_page($view, $data=array()) { // I think this makes more sense
  
    $data['page_template_to_load'] = $view;
    $this->load->view('main', $data);
  }

}

