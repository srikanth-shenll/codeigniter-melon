<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Product extends CI_Controller {
		public function __construct()  {
	          parent::__construct(); 
	          $this->load->database();
	          $this->load->model('Product_model');
	          $this->load->library('form_validation');
	          $this->load->helper('showalertmessage');
	    }

		public function index() {
			    $data['records'] = $this->Product_model->getProducts();
			    $this->_render_page("product/products",$data);
		}

		public function add_products() {
			 if(isset($_POST['Sku_products'])) {
					$Sku_products = stripslashes($_POST['Sku_products']);
					$description_products = stripslashes($_POST['description_products']);
					$datetime=date("Y-m-d H:i:s") ;
					if($Sku_products!="") {
					 	$products= $this->Product_model->getProductBySku($Sku_products);  
						if($products == 0) {
							$this->Product_model->addProduct($Sku_products,$description_products ,$datetime);
							$data=$this->session->set_flashdata('status_message', array('message' => 'Product added successfully','status' => 'success'));
							redirect('product/');

						} else {
							$this->session->set_flashdata('status_message', array('message' => 'Product sku already exists','status' => 'error'));
							redirect('product/add_products');
						}
					} 
			 }else{
			 	 $this->_render_page('product/add_products', array('sku_products'=>'', 'description_products'=>''));
			 }
		}

		public function edit_products($id) {
			if(isset($_POST['Sku_products'])) {
				$Sku_products = stripslashes($_POST['Sku_products']);
				$description_products = stripslashes($_POST['description_products']);
				$datetime=date("Y-m-d H:i:s") ;
				if($Sku_products!="") {
					 $products= $this->Product_model->getProductBySku($Sku_products);
					$updated_sku= $this->Product_model->getProductBySkuId($Sku_products,$id);
					if(($products == 0 ) || ($Sku_products==$updated_sku)) {
						$this->Product_model->updateProduct($Sku_products,$description_products ,$datetime,$id);
						$this->session->set_flashdata('status_message', array('message' => 'Product updated successfully','status' => 'success'));
						redirect('product/');
					} else {
						$this->session->set_flashdata('status_message', array('message' => 'Product sku already exists','status' => 'error'));
						redirect('product/add_products');
					}
				} 
			}else{
				$products= $this->Product_model->selectProductById($id);
				$sku_products = $description_products = '';
				if($products){
		           $sku_products = $products->sku;
		           $description_products = $products->description;
	        	}
	           $this->_render_page('product/add_products', array('sku_products'=>$sku_products, 'description_products'=>$description_products));
			}
		}

		public function delete_products($id) {
		   	$this->load->model("Product_model");
		    $this->Product_model->deleteProduct($id);
		    $this->session->set_flashdata('status_message', array('message' => 'Product deleted successfully','status' => 'success'));
			redirect('product/');  
		}

		public function _render_page($view, $data=array()) { //I think this makes more sense
			$data['page_template_to_load'] = $view;
			$this->load->view('main', $data);
		}
}
