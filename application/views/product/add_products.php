<div id="content">
  <div class="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
      <ul id="breadcrumbs" class="breadcrumb">
        <li>
          <i class="icon-home"></i>
          <a href="#">Dashboard</a>
        </li>
        
        <li class="current">
          <a href="#" title="">Add Products</a>
        </li>
      </ul>
    </div>
    <!-- /Breadcrumbs line -->

    <!--=== Page Header ===-->
    <div class="page-header">
      <div class="page-title">
          <h3><?php echo lang('create_user_heading');?></h3>
        </div>
    </div>
    <!--=== Page Content ===-->
    <div class="row">
      <!--=== Validation Example 1 ===-->
      <div class="col-md-10">
        <div class="widget box">
          <div class="widget-header">
            <h4><i class="icon-reorder"></i> Add Products</h4>
          </div>
          <div class="widget-content">
            <form   class="form-horizontal row-border"  id="product_validate" method="post" aaction="<?php echo base_url().'pages/productlisting'?>">
                <div class="form-body" >
                    <input type="hidden"  value="<?php $url = basename($_SERVER['REQUEST_URI']); ?>">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Product Sku<span class="required">*</span> 
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="Sku_products" id="Sku_products" class="form-control required" value="<?php echo $sku_products;?>">
                            <?php
                                  $message = $this->session->flashdata('item');
                                  if(!empty ($message['message'])) { 
                            ?>
                                     <span style="color:red;"><?php echo $message['message'];?></span>
                           <?php  }  ?> 
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Product Descripton<span class="required">*</span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" name="description_products" id="description_products" class="form-control required" value="<?php echo $description_products;?>">
                             
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-4">
                            <button type="submit" class="btn btn-primary" onclick="return $(this).closest('form').valid();">Submit</button>  
                            <?php if($url=="add_products") { ?> 
                            <input type="button" class="btn btn-danger" value="Cancel" onclick="document.location='<?php echo base_url(); ?>product/productlisting'">                 
                            <?php } else { ?>   
                            <input type="button" class="btn btn-danger" value="Cancel" onclick="document.location='<?php echo base_url(); ?>product/productlisting'">
                            <?php  } ?>

                         </div>
                    </div>
                </div>
            </form>
          </div>
        </div>
        <!-- /Validation Example 1 -->
      </div>
    </div>
    <!-- /Page Content -->
  </div>
  <!-- /.container -->
</div>
