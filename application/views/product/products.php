<div id="content">
    <div class="container">
        <div class="crumbs">
            <ul id="breadcrumbs" class="breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="#">Dashboard</a>
                </li>
                <li class="current">
                    <a href="#" title="">Product Listing</a>
                </li>
            </ul>
        </div>
       
        <div class="page-header">
            <div class="page-title">
                <h3>Listing Tables </h3>
            </div>
            <div class="toolbar" style="float:right;padding: 25px 0px 0;">
                <a href="<?php echo base_url();?>product/add_products">
                    <button type="button" class="btn btn-primary">Add Product</button>
                </a>
            </div>
        </div>
        <?php
            $message = $this->session->flashdata('status_message');
            if(!empty($message['message'])) { 
                echo showAlertMessage($message['status'], $message["message"]);
            }
        ?>
        <div class="row">
            <!--=== Table with Footer ===-->
            <div class="col-md-12">
                <div class="widget box">
                    <div class="widget-header">
                        <h4><i class="icon-reorder"></i> Product Listing's ( From the database ) </h4>
                        <div class="toolbar no-padding">
                            <div class="btn-group">
                                <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="widget-content">
                        <table class="table table-striped table-bordered table-hover table-checkable table-responsive datatable">
                            <thead>
                                <tr>
                                    <th style="text-align:center;">S.No</th>
                                    <th style="text-align:center;">Sku Code</th>
                                    <th style="text-align:center;">Description</th> 
                                    <th style="text-align:center;">Product Type</th>
                                    <th style="text-align: center;">Buttons</th>
                                </tr>
                            </thead>
                            <tbody style="text-align:center">
                                <?php $sno=1; ?>
                                <?php foreach($records as $post) { ?>
                                    <tr>
                                        <td style="text-align:center;"><?php echo $sno;?></td>
                                        <td style="text-align:center;"><?php echo $post->sku;?></td>
                                        <td style="text-align:center;"><?php echo $post->description;?></td> 
                                        <td style="text-align:center;"><?php echo $post->product_type;?></td>
                                        <td style="text-align:center;">  
                                            <span class="btn-group">
                                                <a href="<?php echo base_url().'product/edit_products/'?><?php echo $post->product_id;?>" class="btn btn-xs bs-tooltip" title="" data-original-title="Edit"><i class="icon-pencil"></i></a>
                                                <a href="<?php echo base_url().'product/delete_products/'?><?php echo $post->product_id;?>" class="btn btn-xs bs-tooltip" onclick="return confirm('Are you sure want to delete this product?');" ><i class="icon-trash"></i></a>
                                            </span>
                                        </td>
                                    </tr>    
                                <?php $sno++;} ?> 
                            </tbody>            
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
