<?php
    $this->view('includes/header');
    $this->view('includes/top-menu');
    $this->view('includes/sidebar-menu');
?>
        <div class="row">   
            <div class="col-md-12">
                <!-- BEGIN VALIDATION STATES-->


                <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <a href="#">Edit Products</a>
                                <i class="fa fa-circle"></i>
                            </li>
                        </ul>
                    </div>  


                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-layers font-green"></i>
                            <span class="caption-subject font-green sbold uppercase">Edit Products</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <!-- BEGIN FORM-->
                        <!-- <form class="form-horizontal row-border" id="product_validate" action="#" method="post">  edit_products -->
                        <form class="form-horizontal row-border" id="product_validate" action="<?php echo base_url().'pages/products'?>" method="post">
                            <div class="form-body">
                                <!-- <h3 class="form-section"> Validation Example 1</h3> -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Product Sku<span class="required">*</span> 
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" name="Sku_products" id="Sku_products" class="form-control required">
                                       
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Product Descripton<span class="required">*</span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="text" name="description_products" id="description_products" class="form-control required">
                                        <?php echo form_input(array('id' => 'description_products', 'name' => 'description_products')); ?>
                                    </div>
                                </div>
                            
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-4">
                                        <button type="submit" class="btn green">Submit</button>
                                        <!-- <button type="button" class="btn default">Cancel</button> -->
                                        <input type="button" class="btn default" value="Cancel" onclick="document.location='products'">
                                    </div>
                                </div>
                            </div>

                            </div>
                            <!-- <div class="form-actions">
                                    <input type="submit" value="Submit" class="btn btn-primary">
                                    <input type="button" value="Cancel" class="btn btn-danger">
                                </div> -->

                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
                <!-- END VALIDATION STATES-->
            </div>
        </div>
        </div>

<?php
    $this->view('includes/footer');
?>