<?php
	$this->view('includes/header');
	$this->view('includes/top-menu');
?>
<div id="container" class="fixed-header">
	<?php
		$this->view('includes/sidebar-menu');
	?>
	<?php
		if(!empty($page_template_to_load)){
			$this->view($page_template_to_load);
		}else{
			echo '<h1>No content to load</h1>';
		}
	?>
</div>
<?php
	$this->view('includes/footer');
?>
