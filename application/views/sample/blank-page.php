<div id="content">
	<div class="container">
		<!-- Breadcrumbs line -->
		<div class="crumbs">
			<ul id="breadcrumbs" class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="#">Dashboard</a>
				</li>
				<li class="current">
					<a href="#" title="">Blank</a>
				</li>
			</ul>
		</div>
		<!-- /Breadcrumbs line -->

		<!--=== Page Header ===-->
		<div class="page-header">
			<div class="page-title">
				<h3>Page Heading</h3>
			</div>
		</div>
		<!-- /Page Header -->

		<!--=== Page Content ===-->
		<div class="row">

		</div>
		<!-- /Page Content -->
	</div>
	<!-- /.container -->
</div>
