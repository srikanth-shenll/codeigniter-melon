<div id="content">
	<div class="container">
		<!-- Breadcrumbs line -->
		<div class="crumbs">
			<ul id="breadcrumbs" class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="#">Dashboard</a>
				</li>
				<li class="current">
					<a href="#" title="">Form</a>
				</li>
			</ul>
		</div>
		<!-- /Breadcrumbs line -->

		<!--=== Page Header ===-->
		<div class="page-header">
			<div class="page-title">
 					<h3>Form Validation</h3>
  			</div>
		</div>
		<!-- /Page Header -->

		<!--=== Page Content ===-->
		<div class="row">
			<!--=== Validation Example 1 ===-->
			<div class="col-md-10">
				<div class="widget box">
					<div class="widget-header">
						<h4><i class="icon-reorder"></i> Validation Example 1</h4>
					</div>
					<div class="widget-content">
						<form class="form-horizontal row-border" id="validate-1" action="#" method="post">
							<div class="form-group">
								<label class="col-md-3 control-label">Required <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" name="req1" class="form-control required">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Email <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" name="email1" class="form-control required email">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">URL <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" name="url1" class="form-control required url">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Password <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="password" name="pass1" class="form-control required" minlength="5">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Confirm Password <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="password" name="cpass1" class="form-control required" minlength="5" equalTo="[name='pass1']">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Round Number <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" name="digits1" class="form-control required digits">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Field [min length 5] <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" name="minl1" class="form-control required" minlength="5">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Field [max length 5] <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" name="maxl1" class="form-control required" maxlength="5">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Field [length 5-10] <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" name="rangel1" class="form-control required" rangelength="5, 10">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Number [min 5] <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" name="min1" class="form-control required digits" min="5">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Number [max 5] <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" name="max1" class="form-control required digits" max="5">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Number [5-10] <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="text" name="range1" class="form-control required digits" range="5, 10">
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">File <span class="required">*</span></label>
								<div class="col-md-9">
									<input type="file" name="file1" class="required" accept="image/*" data-style="fileinput" data-inputsize="medium">
									<p class="help-block">Images only (image/*)</p>
									<label for="file1" class="has-error help-block" generated="true" style="display:none;"></label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label">Dropdown <span class="required">*</span></label>
								<div class="col-md-9">
									<select name="dd1" class="form-control required">
										<option value=""></option>
										<option value="1">Option 1</option>
										<option value="2">Option 2</option>
										<option value="3">Option 3</option>
									</select>
								</div>
							</div>
							<div class="form-actions">
								<input type="submit" value="Submit" class="btn btn-primary">
								<input type="button" value="Cancel" class="btn btn-danger">
							</div>
						</form>
					</div>
				</div>
				<!-- /Validation Example 1 -->
			</div>
		</div>
		<!-- /Page Content -->
	</div>
	<!-- /.container -->

</div>
