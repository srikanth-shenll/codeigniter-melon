<div id="content">
	<div class="container">
		<!-- Breadcrumbs line -->
		<div class="crumbs">
			<ul id="breadcrumbs" class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="#">Dashboard</a>
				</li>
				<li class="current">
					<a href="#" title="">Listing</a>
				</li>
			</ul>
		</div>
		<!-- /Breadcrumbs line -->

		<!--=== Page Header ===-->
		<div class="page-header">
			<div class="page-title">
				<h3>Static Tables</h3>
			</div>
		</div>
		<!-- /Page Header -->

		<div class="row">
			<!--=== Table with Footer ===-->
			<div class="col-md-12">
				<div class="widget box">
					<div class="widget-header">
						<h4><i class="icon-reorder"></i> Table with Footer</h4>
						<div class="toolbar no-padding">
							<div class="btn-group">
								<span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
							</div>
						</div>
					</div>
					<div class="widget-content">
						<table class="table table-hover table-striped table-bordered table-highlight-head">
							<thead>
								<tr>
									<th>#</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th class="hidden-xs">Username</th>
									<th>Status</th>
									<th class="align-center">Buttons</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>1</td>
									<td>Joey</td>
									<td>Greyson</td>
									<td class="hidden-xs">joey123</td>
									<td><span class="label label-success">Approved</span></td>
									<td class="align-center">
										<span class="btn-group">
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Search"><i class="icon-search"></i></a>
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Edit"><i class="icon-pencil"></i></a>
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Delete"><i class="icon-trash"></i></a>
										</span>
									</td>
								</tr>
								<tr>
									<td>2</td>
									<td>Wolf</td>
									<td>Bud</td>
									<td class="hidden-xs">wolfy</td>
									<td><span class="label label-info">Pending</span></td>
									<td class="align-center">
										<span class="btn-group">
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Search"><i class="icon-search"></i></a>
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Edit"><i class="icon-pencil"></i></a>
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Delete"><i class="icon-trash"></i></a>
										</span>
									</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Darin</td>
									<td>Alec</td>
									<td class="hidden-xs">alec82</td>
									<td><span class="label label-warning">Suspended</span></td>
									<td class="align-center">
										<span class="btn-group">
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Search"><i class="icon-search"></i></a>
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Edit"><i class="icon-pencil"></i></a>
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Delete"><i class="icon-trash"></i></a>
										</span>
									</td>
								</tr>
								<tr>
									<td>4</td>
									<td>Andrea</td>
									<td>Brenden</td>
									<td class="hidden-xs">andry</td>
									<td><span class="label label-danger">Blocked</span></td>
									<td class="align-center">
										<span class="btn-group">
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Search"><i class="icon-search"></i></a>
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Edit"><i class="icon-pencil"></i></a>
											<a href="javascript:void(0);" class="btn btn-xs bs-tooltip" title="" data-original-title="Delete"><i class="icon-trash"></i></a>
										</span>
									</td>
								</tr>
							</tbody>
						</table>
						<div class="row">
							<div class="table-footer">
								<div class="col-md-6">
									<div class="table-actions">
										<label>Apply action:</label>
										<select class="select2" data-minimum-results-for-search="-1" data-placeholder="Select action...">
											<option value=""></option>
											<option value="Edit">Edit</option>
											<option value="Delete">Delete</option>
											<option value="Move">Move</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<ul class="pagination">
										<li class="disabled"><a href="javascript:void(0);">← Prev</a></li>
										<li class="active"><a href="javascript:void(0);">1</a></li>
										<li><a href="javascript:void(0);">2</a></li>
										<li><a href="javascript:void(0);">3</a></li>
										<li><a href="javascript:void(0);">4</a></li>
										<li><a href="javascript:void(0);">Next →</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /Table with Footer -->
		</div>
		<!-- /Page Content -->
	</div>
	<!-- /.container -->

</div>
