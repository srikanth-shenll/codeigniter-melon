<div class="login" style="min-height:680px;    margin-top: -50px;">
  <div class="logo">
    <strong>LO</strong>GO
  </div>
  <!-- /Logo -->

  <!-- Login Box -->
  <div class="box">
    <div class="content">
      <!-- Login Formular -->
      <?php echo form_open("auth/login", array('id' => 'myform'));?>
        <!-- Title -->
        <h3 class="form-title">Sign In to your Account</h3>
        <?php
            $message = $this->session->flashdata('status_message');
            if(!empty($message['message'])) { 
                echo showAlertMessage($message['status'], $message["message"], true);
            }
        ?>
        <!-- Input Fields -->
        <div class="form-group">
            <div class="input-icon">
                <i class="icon-user"></i>
                <input type="email" name="identity" class="form-control" placeholder="Username" autofocus="autofocus" data-rule-required="true" data-msg-required="Please enter your username." value="<?php echo set_value('username'); ?>" />
            </div>
            <?php echo form_error('username'); ?>
        </div>
        <div class="form-group">
            <div class="input-icon">
                <i class="icon-lock"></i>
                <input type="password" name="password" class="form-control" placeholder="Password" autofocus="autofocus" data-rule-required="true" data-msg-required="Please enter your password." />
            </div>
            <?php echo form_error('password'); ?>
        </div>
        <div class="form-actions">
            <label class="checkbox pull-left"><input type="checkbox" class="uniform" name="remember">Remember me</label>
            <button type="submit" class="submit btn btn-primary pull-right">Sign In <i class="icon-angle-right"></i></button>
        </div>
      <?php echo form_close();?>
      <!-- /Login Formular -->

    </div> <!-- /.content -->


  </div>
</div>

