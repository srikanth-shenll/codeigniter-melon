<div id="content">
  <div class="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
        <ul id="breadcrumbs" class="breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a href="#">Dashboard</a>
            </li>
            <li class="current">
                <a href="#" title="">ChangePassword</a>
            </li>
        </ul>
    </div>
    <!-- /Breadcrumbs line -->

    <!--=== Page Header ===-->
    <div class="page-header">
      <div class="page-title">
          <h3><?php echo lang('change_password_heading');?></h3>
        </div>
    </div>
    <!--=== Page Content ===-->
    <div class="row">
      <!--=== Validation Example 1 ===-->
      <div class="col-md-10">
        <div class="widget box">
            <div class="widget-header">
                  <h4><i class="icon-reorder"></i></h4>
            </div>
            <br>
            <?php
                $message = $this->session->flashdata('status_message');
                if(!empty($message['message'])) { 
                    echo showAlertMessage($message['status'], $message["message"], true);
                }
            ?>
            <div class="widget-content">
                <?php echo form_open("auth/change_password", array('class'=>'form-horizontal row-border'));?>
                    <div class="form-group">
                          <label class="col-md-3 control-label"><?php echo lang('change_password_old_password_label', 'old_password');?>
                                <span class="required">*</span>
                          </label>
                          <div class="col-md-9">
                                <?php echo form_input($old_password);?>
                          </div>
                    </div>
                    <div class="form-group">
                          <label class="col-md-3 control-label"><?php echo sprintf(lang('change_password_new_password_label'), $min_password_length);?>
                                <span class="required">*</span>
                          </label>
                          <div class="col-md-9">
                                <?php echo form_input($new_password);?>
                          </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">
                            <?php echo lang('change_password_new_password_confirm_label', 'new_password_confirm');?>
                            <span class="required">*</span>
                        </label>
                        <div class="col-md-9">
                              <?php echo form_input($new_password_confirm);?>
                        </div>
                    </div>
                    <div class="form-actions">
                        <?php echo form_input($user_id);?>
                        <?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary pull-right'), lang('change_password_submit_btn'), 'onclick="return $(this).closest(\'form\').valid();"');?>
                    </div>
                  <?php echo form_close();?>
            </div>
        </div>
      </div>
    </div>
  </div>
 </div>
