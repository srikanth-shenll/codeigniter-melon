<div id="content">
  <div class="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
      <ul id="breadcrumbs" class="breadcrumb">
        <li>
          <i class="icon-home"></i>
          <a href="#">Dashboard</a>
        </li>
        <li class="current">
          <a href="#" title="">Edit User</a>
        </li>
      </ul>
    </div>
    <!-- /Breadcrumbs line -->

    <!--=== Page Header ===-->
    <div class="page-header">
      <div class="page-title">
          <h3>Update User</h3>
        </div>
    </div>
    <!-- /Page Header -->

    <!--=== Page Content ===-->
    <div class="row">
      <!--=== Validation Example 1 ===-->
      <div class="col-md-10">
        <div class="widget box">
          <div class="widget-header">
            <h4><i class="icon-reorder"></i> Update below user details</h4>
          </div>
          <div class="widget-content">
            <?php
                $message = $this->session->flashdata('status_message');
                if(!empty($message['message'])) { 
                    echo showAlertMessage($message['status'], $message["message"], true);
                }
            ?>
            <?php echo form_open(uri_string(), array('class'=>'form-horizontal row-border'));?>

                  <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo lang('edit_user_fname_label', 'first_name');?> <span class="required">*</span></label>
                    <div class="col-md-9">
                        <?php echo form_input($first_name);?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo lang('edit_user_lname_label', 'last_name');?> <span class="required">*</span></label>
                    <div class="col-md-9">
                        <?php echo form_input($last_name);?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo lang('edit_user_company_label', 'company');?></label>
                    <div class="col-md-9">
                        <?php echo form_input($company);?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo lang('edit_user_phone_label', 'phone');?>
                      </label>
                    <div class="col-md-9">
                        <?php echo form_input($phone);?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo lang('edit_user_password_label', 'password');?>
                      </label>
                    <div class="col-md-9">
                        <?php echo form_input($password);?>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo lang('edit_user_password_confirm_label', 'password_confirm');?>
                      </label>
                    <div class="col-md-9">
                        <?php echo form_input($password_confirm);?>
                    </div>
                  </div>

                  <?php if ($this->ion_auth->is_admin()): ?>
                    <div class="form-group">
                    <label class="col-md-3 control-label"><?php echo lang('edit_user_groups_heading');?></label>
                    <div class="col-md-9">
                      <?php foreach ($groups as $group):?>
                          <label class="checkbox">
                          <?php
                              $gID=$group['id'];
                              $checked = null;
                              $item = null;
                              foreach($currentGroups as $grp) {
                                  if ($gID == $grp->id) {
                                      $checked= ' checked="checked"';
                                      break;
                                  }
                              }
                          ?>
                          <input type="checkbox" name="groups[]" value="<?php echo $group['id'];?>"<?php echo $checked;?>>
                          <?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?>
                          </label>
                      <?php endforeach?>
                      </div>
                  </div>
                  <?php endif ?>

                  <?php echo form_hidden('id', $user->id);?>
                  <?php echo form_hidden($csrf); ?>
                  <div class="form-actions">
                    <?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary pull-right'), lang('edit_user_submit_btn'), 'onclick="return $(this).closest(\'form\').valid();"');?>
                  </div>
            <?php echo form_close();?>
          </div>
        </div>
        <!-- /Validation Example 1 -->
      </div>
    </div>
    <!-- /Page Content -->
  </div>
  <!-- /.container -->
</div>
