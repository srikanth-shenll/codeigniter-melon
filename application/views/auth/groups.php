<div id="content">
  <div class="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
      <ul id="breadcrumbs" class="breadcrumb">
        <li>
          <i class="icon-home"></i>
          <a href="#">Dashboard</a>
        </li>
        <li class="current">
          <a href="#" title="">Groups</a>
        </li>
      </ul>
    </div>
    <!-- /Breadcrumbs line -->

    <!--=== Page Header ===-->
    <div class="page-header">
      <div class="page-title">
          <h3>Groups List</h3>
        </div>
    </div>
    <!--=== Page Content === -->
      <?php
            $message = $this->session->flashdata('item');
            if(!empty ($message['message'])) { ?>
                <div class="alert alert-success fade in">
                    <a class="close" data-dismiss="alert">X</a>
                    <?php echo $message['message'];?>
                </div>
      <?php   }  ?>
      <?php                                  
          $error_message = $this->session->flashdata('error');
          if(!empty ($error_message['message'])) { ?>
              <div class="custom-alerts alert alert-danger fade in">
                  <a class="close" data-dismiss="alert">&times;</a>
                      <?php echo $error_message['message'];?>
              </div>
       <?php    }
        ?>
    <div class="row">
      <!--=== Validation Example 1 ===-->
      <div class="col-md-12">
        <div class="widget box">
          <div class="widget-header">
            <h4><i class="icon-reorder"></i> Below is list of groups</h4>
            <div class="toolbar no-padding">
              <div class="btn-group">
                <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
              </div>
            </div>
          </div>
          <div class="widget-content">
            <table class="table table-striped table-bordered table-hover table-checkable datatable">
              <thead>
                <tr>
                  <th>#</th>
                 <!--  <th>Group ID</th> -->
                  <th>Group Name</th>
                  <th>Description</th>
                  <th class="align-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  if(!empty($groups)){
                    $sno = 1;
                    foreach($groups as $group){
                      $html = '';
                      $html .= '<tr>';
                      $html .= '<td>'.$sno.'</td>';
                      // $html .= '<td>'.$group->id.'</td>';
                      $html .= '<td>'.$group->name.'</td>';
                      $html .= '<td>'.$group->description.'</td>';
                      $editUrl = base_url(sprintf('auth/edit_group/%d', $group->id));
                      $deleteUrl = base_url(sprintf('auth/delete_group/%d', $group->id));
                      $confirmUrl="return confirm('Are you sure want to delete this group ?')";
                      $html .= '<td class="align-center">
                                  <span class="btn-group">
                                    <a href="'.$editUrl.'" class="btn btn-xs bs-tooltip" title="" data-original-title="Edit"><i class="icon-pencil"></i></a>
                                    <a href="'.$deleteUrl.'" class="btn btn-xs bs-tooltip" onclick="'.$confirmUrl.'"> <i class="icon-trash"></i> </a>
                                  </span>
                                </td>';
                      $html .= '</tr>';
                      $sno++;
                      echo $html;
                    }

                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /Page Content -->
  </div>
  <!-- /.container -->

</div>
