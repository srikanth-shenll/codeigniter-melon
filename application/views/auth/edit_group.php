<div id="content">
  <div class="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
      <ul id="breadcrumbs" class="breadcrumb">
        <li>
          <i class="icon-home"></i>
          <a href="#">Dashboard</a>
        </li>
        <li class="current">
          <a href="#" title="">Edit Group</a>
        </li>
      </ul>
    </div>
    <!-- /Breadcrumbs line -->

    <!--=== Page Header ===-->
    <div class="page-header">
      <div class="page-title">
          <h3><?php echo lang('edit_group_heading');?></h3>
        </div>
    </div>
    <!--=== Page Content ===-->
    <div class="row">
      <!--=== Validation Example 1 ===-->
      <div class="col-md-10">
        <div class="widget box">
          <div class="widget-header">
            <h4><i class="icon-reorder"></i> <?php echo lang('edit_group_subheading');?></h4>
          </div>
          <div class="widget-content">
              <?php
                  $message = $this->session->flashdata('status_message');
                  if(!empty($message['message'])) { 
                      echo showAlertMessage($message['status'], $message["message"], true);
                  }
              ?>
              <?php echo form_open(current_url(), array('class'=>'form-horizontal row-border'));?>
              <div class="form-group">
                  <label class="col-md-3 control-label"><?php echo lang('edit_group_name_label', 'group_name');?><span class="required">*</span></label>
                  <div class="col-md-9"><?php echo form_input($group_name);?></div> 
              </div>
              <div class="form-group">
                  <label class="col-md-3 control-label"><?php echo lang('edit_group_desc_label', 'description');?></label>
                  <div class="col-md-9"><?php echo form_input($group_description);?></div>
              </div>
              <div class="form-actions">
                <?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary pull-right'),lang('edit_group_submit_btn'), 'onclick="return $(this).closest(\'form\').valid();"');?> 
              </div>
              <?php echo form_close();?>
          </div>
        </div>
        <!-- /Validation Example 1 -->
      </div>
    </div>
    <!-- /Page Content -->
  </div>
  <!-- /.container -->
</div>
