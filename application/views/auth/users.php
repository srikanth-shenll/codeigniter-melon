<div id="content">
  <div class="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
      <ul id="breadcrumbs" class="breadcrumb">
        <li>
          <i class="icon-home"></i>
          <a href="#">Dashboard</a>
        </li>
        <li class="current">
          <a href="#" title="">Users</a> 
        </li>
      </ul>
    </div>
    <!-- /Breadcrumbs line -->

    <!--=== Page Header ===-->
    <div class="page-header">
        <div class="page-title">
            <h3>Users List</h3>
        </div>
    </div>
    <?php
        $message = $this->session->flashdata('status_message');
        if(!empty($message['message'])) { 
            echo showAlertMessage($message['status'], $message["message"], true);
        }
    ?>

    <!--=== Page Content ===-->
    <div class="row">
      <!--=== Validation Example 1 ===-->
      <div class="col-md-12">
        <div class="widget box">
          <div class="widget-header">
            <h4><i class="icon-reorder"></i> Below is list of users</h4>
            <div class="toolbar no-padding">
              <div class="btn-group">
                <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
              </div>
            </div>
          </div>
          <div class="widget-content">
            <table class="table table-hover table-striped table-bordered table-highlight-head">
              <thead>
                <tr>
                  <th>#</th>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th class="hidden-xs">Username</th>
                  <th>Status</th>
                  <th class="align-center">Actions</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  if(!empty($users)){
                    $sno = 1;
                    foreach($users as $user){
                      $html = '';
                      $html .= '<tr>';
                      $html .= '<td>'.$sno.'</td>';
                      $html .= '<td>'.$user->first_name.'</td>';
                      $html .= '<td>'.$user->last_name.'</td>';
                      $html .= '<td>'.$user->email.'</td>';
                      $html .= '<td>'.$user->username.'</td>';
                      $html .= '<td>'.(($user->active)?'<span class="label label-success">Active</span>':'<span class="label label-danger">In-active</span>').'</td>';
                      $editUrl    = base_url(sprintf('auth/edit_user/%d', $user->id));
                      $deleteUrl  = base_url(sprintf('auth/delete/%d', $user->id));
                      $confirmUrl="return confirm('Are you sure want to delete this user ?')"; 
                      $html .= '<td class="align-center">
                                  <span class="btn-group">
                                      <a href="'.$editUrl.'" class="btn btn-xs bs-tooltip" title="" data-original-title="Edit">
                                      <i class="icon-pencil"></i></a>
                                      <a href="'.$deleteUrl.'" class="btn btn-xs bs-tooltip" title="" data-original-title="Delete" onclick="'.$confirmUrl.'"><i class="icon-trash"></i> </a>
                                  </span>
                                </td>';
                      $html .= '</tr>';
                      $sno++;
                      echo $html;
                    }

                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <!-- /Page Content -->
  </div>
  <!-- /.container -->

</div>
