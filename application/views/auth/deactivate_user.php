<div id="content">
  <div class="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
      <ul id="breadcrumbs" class="breadcrumb">
        <li>
          <i class="icon-home"></i>
          <a href="index.html">Dashboard</a>
        </li>
        <li class="current">
          <a href="pages_calendar.html" title="">Calendar</a>
        </li>
      </ul>
    </div>
    <!-- /Breadcrumbs line -->

    <!--=== Page Header ===-->
    <div class="page-header">
      <div class="page-title">
          <h3>Form Validation</h3>
        </div>
    </div>
    <!--=== Page Content ===-->
    <div class="row">
      <!--=== Validation Example 1 ===-->
      <div class="col-md-10">
        <div class="widget box">
          <div class="widget-header">
            <h4><i class="icon-reorder"></i> Validation Example 1</h4>
          </div>
          <div class="widget-content">
                <h1><?php echo lang('deactivate_heading');?></h1>
                <p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>

                <?php echo form_open("auth/deactivate/".$user->id);?>

                  <p>
                  	<?php echo lang('deactivate_confirm_y_label', 'confirm');?>
                    <input type="radio" name="confirm" value="yes" checked="checked" />
                    <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
                    <input type="radio" name="confirm" value="no" />
                  </p>

                  <?php echo form_hidden($csrf); ?>
                  <?php echo form_hidden(array('id'=>$user->id)); ?>

                  <p><?php echo form_submit('submit', lang('deactivate_submit_btn'));?></p>

                <?php echo form_close();?>
            </div>
        </div>
        <!-- /Validation Example 1 -->
      </div>
    </div>
    <!-- /Page Content -->
  </div>
  <!-- /.container -->

</div>

