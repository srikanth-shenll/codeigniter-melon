<div id="content">
  <div class="container">
    <!-- Breadcrumbs line -->
    <div class="crumbs">
      <ul id="breadcrumbs" class="breadcrumb">
        <li>
          <i class="icon-home"></i>
          <a href="#">Dashboard</a>
        </li>
        <li class="current">
          <a href="#" title="">Create User</a>
        </li>
      </ul>
    </div>
    <!-- /Breadcrumbs line -->

    <!--=== Page Header ===-->
    <div class="page-header">
      <div class="page-title">
          <h3><?php echo lang('create_user_heading');?></h3>
        </div>
    </div>
    <!--=== Page Content ===-->
    <div class="row">
      <!--=== Validation Example 1 ===-->
      <div class="col-md-10">
        <div class="widget box">
          <div class="widget-header">
                <h4><i class="icon-reorder"></i> <?php echo lang('create_user_heading');?></h4>
          </div>
          <div class="widget-content">
            <?php
                $message = $this->session->flashdata('status_message');
                if(!empty($message['message'])) { 
                    echo showAlertMessage($message['status'], $message["message"], true);
                }
            ?>
            <?php echo form_open("auth/create_user", array('class'=>'form-horizontal row-border'));?>

              <div class="form-group">
                <label class="col-md-3 control-label">
                      <?php echo lang('create_user_fname_label', 'first_name');?>
                      <span class="required">*</span>
                </label>
                <div class="col-md-9">
                  <?php echo form_input($first_name);?>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label"> <?php echo lang('create_user_lname_label', 'last_name');?>
                      <span class="required">*</span>
                </label>
                <div class="col-md-9">
                    <?php echo form_input($last_name);?>
                </div>
              </div>

              <?php
              if($identity_column!=='email') {
                  echo '<p>';
                  echo lang('create_user_identity_label', 'identity');
                  echo '<br />';
                  echo form_error('identity');
                  echo form_input($identity);
                  echo '</p>';
              }
              ?>

              <div class="form-group">
                <label class="col-md-3 control-label"> <?php echo lang('create_user_company_label', 'company');?>
                </label>
                <div class="col-md-9">
                    <?php echo form_input($company);?>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label"><?php echo lang('create_user_email_label', 'email');?>
                      <span class="required">*</span>
                </label>
                <div class="col-md-9">
                    <?php echo form_input($email);?>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label"><?php echo lang('create_user_phone_label', 'phone');?>
                </label>
                <div class="col-md-9">
                    <?php echo form_input($phone);?>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label"><?php echo lang('create_user_password_label', 'password');?>
                      <span class="required">*</span>
                </label>
                <div class="col-md-9">
                    <?php echo form_input($password);?>
                </div>
              </div>

             <div class="form-group">
                <label class="col-md-3 control-label"><?php echo lang('create_user_password_confirm_label', 'password_confirm');?>
                      <span class="required">*</span>
                </label>
                <div class="col-md-9">
                    <?php echo form_input($password_confirm);?>
                </div>
              </div>


              <div class="form-actions">
                <?php echo form_submit(array('name'=>'submit', 'class'=>'btn btn-primary pull-right'), lang('create_user_submit_btn'), 'onclick="return $(this).closest(\'form\').valid();"');?>
              </div>
              <?php echo form_close();?>
          </div>
        </div>
        <!-- /Validation Example 1 -->
      </div>
    </div>
    <!-- /Page Content -->
  </div>
  <!-- /.container -->

</div>
