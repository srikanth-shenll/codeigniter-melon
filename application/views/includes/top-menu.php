<?php
	if($this->ion_auth->logged_in()){
?>
<header class="header navbar navbar-fixed-top" role="banner">
	<!-- Top Navigation Bar -->
	<div class="container">

		<!-- Only visible on smartphones, menu toggle -->
		<ul class="nav navbar-nav">
			<li class="nav-toggle"><a href="javascript:void(0);" title=""><i class="icon-reorder"></i></a></li>
		</ul>

		<!-- Logo -->
		<a class="navbar-brand" href="<?php echo site_url();?>">
			<img src="<?php echo base_url();?>assets/img/logo.png" alt="logo">
			<strong>ME</strong>LON
		</a>
		<!-- /logo -->

		<!-- Sidebar Toggler -->
		<!--<a href="#" class="toggle-sidebar bs-tooltip" data-placement="bottom" data-original-title="Toggle navigation">
			<i class="icon-reorder"></i>
		</a> -->
		<!-- /Sidebar Toggler -->

		<!-- Top Right Menu -->
		<ul class="nav navbar-nav navbar-right">
			<!-- User Login Dropdown -->
			<li class="dropdown user">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<!--<img alt="" src="assets/img/avatar1_small.jpg" />-->
					<i class="icon-male"></i>
					<span class="username">
						<?php
							$fnm = $this->session->userdata('first_name');
							echo !empty($fnm)?$fnm:'';
						?>
						&nbsp;
						<?php
							$lnm = $this->session->userdata('last_name');
							echo !empty($lnm)?$lnm:'';
						?>
					</span>
					<i class="icon-caret-down small"></i>
				</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo base_url("auth/profile");?>"><i class="icon-user"></i> My Profile</a></li>
					<li class="divider"></li>
					<li><a href="<?php echo base_url("auth/change_password");?>"><i class="icon-key"></i> Change Password</a></li>
					<li class="divider"></li>
					<li><a href="<?php echo base_url("auth/logout");?>"><i class="icon-power-off"></i> Log Out</a></li>
				</ul>
			</li>
			<!-- /user login dropdown -->
		</ul>
		<!-- /Top Right Menu -->
	</div>
	<!-- /top navigation bar -->
</header>
<?php }?>
