    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/libs/lodash.compat.min.js"></script>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="<?php echo base_url();?>assets/js/libs/html5shiv.js"></script>
    <![endif]-->

    <!-- Smartphone Touch Events -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/touchpunch/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/event.swipe/jquery.event.move.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/event.swipe/jquery.event.swipe.js"></script>

    <!-- General -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/libs/breakpoints.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/respond/respond.min.js"></script>
    <!-- Polyfill for min/max-width CSS3 Media Queries (only for IE8) -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/cookie/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/slimscroll/jquery.slimscroll.horizontal.min.js"></script>

    <!-- Page specific plugins -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/flot/jquery.flot.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/flot/jquery.flot.resize.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/flot/jquery.flot.time.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/flot/jquery.flot.growraf.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/blockui/jquery.blockUI.min.js"></script>

    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar.min.js"></script>

    <!-- Noty -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/noty/jquery.noty.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/noty/layouts/top.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/noty/themes/default.js"></script>

    <!-- Forms -->
    <!-- <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/select2/select2.min.js"></script> -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
    
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/DT_bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/datatables/responsive/datatables.responsive.js"></script> <!-- optional -->


    <!-- App -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/app.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/plugins.form-components.js"></script>

    <!-- Beautiful Checkboxes -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/uniform/jquery.uniform.min.js"></script>

    <!-- Form Validation -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/validation/jquery.validate.min.js"></script>

    <!-- Slim Progress Bars -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/nprogress/nprogress.js"></script>

    <!-- App -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/login.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/demo/form_validation.js"></script>

<!--    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/scripts/form-validation.js" ></script>
 -->         
<!--    <script type="text/javascript" src="<?php echo base_url();?>assets/pages/scripts/table-datatables-rowreorder.min.js" ></script>
 -->
<!--     <script type="text/javascript" src="<?php echo base_url();?>assets/pages/scripts/table-datatables-ajax.min.js" ></script>
 -->
    <script>
    $(document).ready(function(){
        $("#myform").validate();
    });
    </script>
    <script>
    $(document).ready(function(){
        "use strict";

        App.init(); // Init layout and core plugins
        Plugins.init(); // Init all plugins
        FormComponents.init(); // Init all form-specific plugins
    });
    </script>
</body>
</html>
