<?php
if($this->ion_auth->logged_in()){
	$side_menu = $this->config->item('u_sidemenu');
?>
<div id="sidebar" class="sidebar-fixed">
	<div class="slimScrollDiv">
		<div id="sidebar-content">

			<!--=== Navigation ===-->
			<ul id="nav">
				<?php
				  $CurrentActiveMenu = $this->session->flashdata('currentMenuitem');
				  !empty($CurrentActiveMenu)?base_url($CurrentActiveMenu):'';
					if(!empty($side_menu)){
						$openSubCurrent = $printItem = '';
						$currentPage = current_url();
						foreach($side_menu as $menuItem){
							$subMenuItems = '';
							$mUrl = !empty($menuItem['url'])?base_url($menuItem['url']):'javascript:void(0)';
							$mIcon = !empty($menuItem['icon'])?$menuItem['icon']:'icon-dashboard';
							$childSelected = false;
							if(!empty($menuItem['group'])){
								$group = $menuItem['group'];
								if (!$this->ion_auth->in_group($group)){
									continue;
								}
							}
							//Submenu
							if(!empty($menuItem['sub_menu'])){
								$subMenuItems = '<ul class="sub-menu">';
								foreach($menuItem['sub_menu'] as $subMenuItem){
									$smUrl = !empty($subMenuItem['url'])?base_url($subMenuItem['url']):'javascript:void(0)';
									$selectSubCurrent = ($currentPage == $smUrl)?'current':'';
									$selectSubCurrent = (!empty($CurrentActiveMenu) && base_url($CurrentActiveMenu) == $smUrl)?'current':$selectSubCurrent;
									if(!empty($subMenuItem['group'])){
										$subMenugroup = $subMenuItem['group'];
										if (!$this->ion_auth->in_group($subMenugroup)){
											continue;
										}
									}
									if($childSelected === false && !empty($selectSubCurrent)){
										$childSelected = true;
									}
									$openSubCurrent = (!empty($selectSubCurrent) && empty($openSubCurrent))?' open':$openSubCurrent;
									$subMenuItems .= '<li class="'.$selectSubCurrent.'">';
									$subMenuItems .= '<a href="'.$smUrl.'">';
									$subMenuItems .= '<i class="icon-angle-right"></i>';
									$subMenuItems .= !empty($subMenuItem['label'])?$subMenuItem['label']:'';
									$subMenuItems .= '</a>';
									$subMenuItems .= '</li>';
								}
								$subMenuItems .= '</ul>';
							}
							//Main item
							$setParentClass = ($childSelected === true)?'current open':'';
							$printItem .= '<li class="'.$setParentClass.'">';
							$printItem .= '<a href="'.$mUrl.'">';
							$printItem .= '<i class="'.$mIcon.'"></i>';
							$printItem .= !empty($menuItem['label'])?$menuItem['label']:'';
							$printItem .= '</a>';
							$printItem .= $subMenuItems;
							$printItem .= '</li>';
						}
					    echo $printItem;
					}
					$this->session->set_flashdata("currentMenuitem", '');
				?>

			</ul>

			<div class="fill-nav-space"></div>
		</div>
	</div>
	<div id="divider" class="resizeable"></div>
</div>
<!-- /Sidebar -->
<?php }?>
