<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 +-++-++-++-++-++-+ +-++-++-++-++-++-++-++-+ +-++-++-++-++-++-++-++-++-+
 |S||H||E||N||L||L| |S||O||F||T||W||A||R||E| |S||O||L||U||T||I||O||N||S|
 +-++-++-++-++-++-+ +-++-++-++-++-++-++-++-+ +-++-++-++-++-++-++-++-++-+
*/

/** 
User defined configuration 
1. Add a custom configuarion in this file
2. Configuration in this file is independent of ENVIRONMENT
**/
/** User defined configuration **/
$config['u_site_title'] = 'Shenll Dashboard';
$config['u_after_login'] = 'sample/form';

// For side menu
$config['u_sidemenu'] = array(
                                array(
                                    'url'=>'',
                                    'label'=>'Dashboard',
                                    'selected'=>'',
                                    'class'=>'',
                                    'icon'=>'icon-dashboard',
                                    'group'=> ''
                                ),
                                array(
                                    'url'=>'',
                                    'label'=>'Sample Menu',
                                    'selected'=>'',
                                    'class'=>'',
                                    'icon'=>'icon-desktop',
                                    'group'=> '',
                                    'sub_menu'=>array(
                                                    array(
                                                       'url'=>'sample/form',
                                                       'label'=>'Form Example',
                                                       'class'=>'',
                                                       'selected'=>'',
                                                    ),
                                                    array(
                                                       'url'=>'sample/listing',
                                                       'label'=>'Listing Example',
                                                       'class'=>'',
                                                    ),
                                                    array(
                                                       'url'=>'sample/blank',
                                                       'label'=>'Blank Page',
                                                       'class'=>'',
                                                    )
                                                )
	                            ),
	                            array(
		                            'url'=>'',
		                            'label'=>'Products',
		                            'selected'=>'',
		                            'class'=>'',
		                            'icon'=>'icon-group',
		                            'group'=> array('admin', 'members'),
		                            'sub_menu'=>array(
		                                            array(
		                                              	'url'=>'product',
                                                       'label'=>'Product Listing',
		                                               'class'=>''
		                                            )
		                                         )
		                        ),
	                        	array(
		                            'url'=>'',
		                            'label'=>'Users',
		                            'selected'=>'',
		                            'class'=>'',
		                            'icon'=>'icon-group',
		                            'group'=> array('admin'),
		                            'sub_menu'=>array(
		                                            array(
		                                               'url'=>'auth/users',
		                                               'label'=>'Users List',
		                                               'class'=>'',
		                                               'selected'=>'',
		                                            ),
		                                         
		                                            array(
		                                               'url'=>'auth/create_user',
		                                               'label'=>'Create User',
		                                               'class'=>'',
		                                            ),
		                                            array(
		                                               'url'=>'auth/create_group',
		                                               'label'=>'Create Group',
		                                               'class'=>'',
		                                            ),
		                                            array(
		                                               'url'=>'auth/groups',
		                                               'label'=>'Groups List',
		                                               'class'=>'',
		                                            )
		                                        )
		                        ),
                            );
