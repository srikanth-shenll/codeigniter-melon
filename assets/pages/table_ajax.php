<!-- ***** --->
<?php
  /* 
   * Paging
   */   
  $iTotalRecords = 178;
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  //$sEcho = intval($_REQUEST['draw']);
  $records = array();
  $records["data"] = array(); 
  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
  $status_list = array (
	array("success" => "Pending"),
    array("info" => "Closed"),
	array("danger" => "On Hold"),
    array("warning" => "Fraud")
  );
  for($i = $iDisplayStart; $i < $end; $i++) {
		$id = ($i + 1);
		$records["data"][] = array (
		  $id,
		  '201320132013',
		  'Jhon Doe',
		  'Action User',
		  '45060',
		); // pass custom message(getting status of group actions)
  }
  if (isset($_REQUEST["customActionType"]) && isset($_REQUEST["customActionType"]) == "group_action") {
	$records["customActionStatus"] = "OK"; 
	// pass custom message(useful for getting status of group actions) 
	$records["customActionMessage"] = "Group action successfully has been completed. Well done!"; 
	// pass custom message(useful for getting status of group actions)
  }
// $records["draw"] = $sEcho;
  $records["recordsTotal"]		= $iTotalRecords;
  $records["recordsFiltered"]	= $iTotalRecords;
  echo json_encode($records);
 ?>
