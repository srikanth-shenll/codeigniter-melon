Note: 

1. Don't clone master. Use the version branch
2. If you face any issue or need any changes to the code report them in Issues

###################
Setting up the project
###################
`git clone -b <branchname> --single-branch <repo-url>`


1. Clone this project to your local directory
2. Create a DB in MYSQL and import the sample DB.(see the link below)
3. Update the baseurl in *config.php* of environment folder to the cloned path 

$config['base_url'] = 'http://localhost/codeigniter-melon/'; 


4. Update your DB details in *database.php* of environment folder
5. Update the **.htaccess** with the cloned directory name
6. Now load the URL in your browser
7. login with U: admin@admin.com P: password

###################
Sample code
###################

1. We have added Sample code in the admin to demonstrate. There are two sample controllers in the controllers folder and one in model folder. 

`-> Controllers`
`* Sample`
`* Products`

`-> Models`
`* ProductModel`

`-> Views`
`* product folder`
`* sample folder`

2. In the DB there is a sample table **tbl_products**.

**We should remove these sample code and table. They are only for demo purpose**


###################
Authentication - ION AUTH
###################
1. For authentication we are using ion auth(https://github.com/benedmunds/CodeIgniter-Ion-Auth) Version: 2.5.2 library for codeigniter
2. Please go through their documentation for the authentication functions

###################
Composer support
###################
1. We can use composer for this project


###################
List of plugins used
###################
`https://drive.google.com/file/d/0B_DZ6hl8OqxWUThDSU1OTnZ2Yk0/view?usp=sharing`

###################
Sample Database
###################
All tables are needed except tbl_products
`https://drive.google.com/file/d/0B_DZ6hl8OqxWd05wRWstMnhkVGc/view?usp=sharing`